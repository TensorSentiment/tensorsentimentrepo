import requests
import codecs
import time
from bs4 import BeautifulSoup
from Vk_api import filter_text, pr


def get_url(film_id, flag, page):
    return 'https://www.kinopoisk.ru/film/%d/ord/date/status/%s/perpage/200/page/%d/#list' \
           % (film_id, 'good' if flag else 'bad', page)


def parse_reviews(html, flag):
    with pr("         Parse"):
        soup = BeautifulSoup(html, 'lxml')
        review_list = soup.findAll('div', {'class': 'reviewItem userReview'})
        return [get_review(r, flag) for r in review_list]


def simple_text_filter(text):
    return text.replace('\n', ' ').replace('\r', ' ').replace('<br>', ' ')


def get_review(review, flag):
    text = review.find('span', {'itemprop' : 'reviewBody'}).text
    date = review.find('span', {'class' : 'date'}).text

    return "{mark}\t{date}\t{text}\n".format(mark='1' if flag else '-1',
                                           date=date,
                                           text=simple_text_filter(text))#text=filter_text(text))


def get_reviews(film_id, flag):
    reviews = []
    num_page = 0

    """while True:
        r = None
        parsed = None

        with pr("         GET"):
            num_page += 1
            url = get_url(film_id, flag, num_page)
            try:
                r = requests.get(url)
                parsed = parse_reviews(r.text, flag)
            except Exception:
                pass

        if not parsed:
            break

        reviews.extend(parsed)
        print("Page: %s" % num_page)
        time.sleep(7)"""

    with open('bad13.html', 'r') as file:
        reviews.extend(parse_reviews(file.read(), flag))

    return reviews


def get_good(film_id):
    with pr("   Good"):
        return get_reviews(film_id, True)


def get_bad(film_id):
    with pr("   Bad"):
        return get_reviews(film_id, False)


def save_to_file(rev, id, suff):
    with pr("   Save"):
        with codecs.open('Kinopoisk_%d_%s.csv' % (id, suff), "w", "utf-8") as file:
            for r in rev:
                file.write(r)


def save_reviews(film_id):
    with pr("Main"):
        save_to_file(get_bad(film_id), film_id, "bad")
        #save_to_file(get_good(film_id), film_id, "good")


save_reviews(800013)