import tweepy
from nltk.tokenize import word_tokenize
import string
import  pymorphy2
from datetime import datetime, date, time

morph = pymorphy2.MorphAnalyzer()
auth = tweepy.OAuthHandler('xpxa6fLrXIpbPGLa1EfrXXa0n', 'pIQrzXHuQHNiYaZydxY6Mq26CVZfC8gAOLqRz2PBJkjBPK6qk7')
auth.set_access_token('799704699683291136-yG89U77liRF51TE6v1Natfo5j25QbGX', 'rFmnPKtQnDovxXWJfACTpCJZNkNLhgxgQmQb0QCNMqeet')
api = tweepy.API(auth)
film="Мстители"
result=tweepy.Cursor(api.search, q=film).items(1000)
translate_table = dict((ord(char), None) for char in string.punctuation)

massiv_id=[]
massiv_date=[]
massiv_text=[]

#создаём отдельно массивы id, date,text
for i in result:
    massiv_id.append(str(i.id))
    massiv_date.append(str(i.created_at))
    massiv_text.append(str(i.text))


#Приведение массива слов к нормальной форме
def get_normal_form(array_words):
    new_array=[]
    for i in  array_words:
        p= morph.parse(i)[0].normal_form
        new_array.append(p)
        #print(i)
    return new_array

#Преобразование массива в строку
def open_massiv(array_words):
    return ' '.join(array_words)

#фильтрация текста
def filter_text(text):
    return get_normal_form(word_tokenize(text.translate(translate_table)))

#Приведение к форме(id, дата, текст поста)
def filter(massiv_id,massiv_date, massiv_text, count):
    return '%s\t%s\t%s\n' % (str(massiv_id[count]),str(massiv_date[count]),open_massiv(filter_text(str(massiv_text[count]))))

name_new_base = 'list_tweeter_base_%s.csv' % datetime.now().strftime("%d-%m-%Y %H-%M-%S")
with open(name_new_base, 'w', encoding='utf-8') as g:
    for i in range(0,len(massiv_id), 1):
        g.write(filter(massiv_id,massiv_date,massiv_text,i))
