#Входной параметр - искомая строка. Например, '#Дуэлянт'.
#Выходной - csv с колонками: ИД пользователя, дата, текст новости.
#Достаточно 900-1000 записей.

import vk
from nltk.tokenize import word_tokenize
import string
import pymorphy2
from datetime import datetime, date, time

import time

class pr(object):
   def __init__(self, name):
      self.name = name

   def __enter__(self):
      self._startTime = time.time()

   def __exit__(self, type, value, traceback):
      print (self.name + ": {:.3f} sec".format(time.time() - self._startTime))



#Задание символов пунктуации
translate_table = dict((ord(char), None) for char in string.punctuation)


# Получаем посты с помощью vk.api
def search_posts(name_film):
    api = vk.API(vk.Session())
    result = []

    for i in range(0, 1000, 200):
        r = api.newsfeed.search(q = name_film ,count=200, offset=i)[1:]
        result.extend(r)

    return result


# фильтрация текста
def filter_text(text):
    return get_normal_form(word_tokenize(text.translate(translate_table)))


# Приведение массива слов к нормальной форме
def get_normal_form(array_words):
    morph = pymorphy2.MorphAnalyzer()

    new_array = []
    for i in  array_words:
        new_array.append(morph.parse(i)[0].normal_form)
    return ' '.join(new_array)


# Приведение к форме(id, дата, текст поста)
def filter(post):
    return '%s\t%s\t%s\n' % (post.get('owner_id'),
                             datetime.fromtimestamp(post.get('date')).strftime('%d-%m-%Y %H:%M:%S'),
                             filter_text(post.get('text').replace('<br>',' ').replace('#', ' ')))


def save_vk_news(query):
    with pr("Search") as p:
        news = search_posts(query)

    with pr("Save") as p:
        #Запись данных в файл
        name_new_base = 'list_vk_base_%s.csv' % datetime.now().strftime("%d-%m-%Y %H-%M-%S")

        with open(name_new_base, 'w', encoding='utf-8') as g:
            for i in news:
                test=filter(i)
                if(test[0] != '-'):
                    g.write(test)


#save_vk_news("#Дуэлянт")


