from flask import Flask, render_template, request


app = Flask(__name__)

def percent(good, bad):
    percentgood=int(good/(good+bad)*100)
    #percentbad=100-pe
    return percentgood

def RequestModel(message, is_film_name):
    if is_film_name:
        return 1525, 5478
    else:
        return False

@app.route('/', methods=['POST', 'GET'])
def hello_world():
    if request.method == 'POST':
        if request.form.get("condition", None)=='0':
            return render_template("Form.html", condition=0, CheckedRadioFilm="checked")
        elif request.form.get("condition", None)=='1':
            return render_template("Form.html", condition=1, CheckedRadioFilm="checked")
        if request.form.get("seachFilm", None):
            good, bad = RequestModel(request.form["seachFilm"], True)
            sFilm=request.form["seachFilm"]
            percent1=percent(good, bad)
            return render_template('Form.html', condition=4, ResultFilm=percent1, FilmS=sFilm, CheckedRadioFilm="checked", goodQty=good, badQty=bad)
        else:
            sMessage=request.form["AnalyseMessage"]
            analyse=RequestModel(request.form["AnalyseMessage"], False)
            return render_template('Form.html', condition=3, ResultMessage=analyse, Message=sMessage, CheckedRadioMessage="checked")


    return render_template("Form.html", condition = 0, CheckedRadioFilm="checked")



if __name__ == '__main__':
    app.run()
